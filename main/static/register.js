$(document).ready(function() {
    var username_valid = false
    var email_valid = false
    var password_valid = false

    // Homepage
    $("#register-goto").click(function() {
        $('#login-div').hide();
        $('#register-div').show();
    });

    $("#login-goto").click(function() {
        $('#register-div').hide();
        $('#login-div').show();
    });

    //Register Validation
    function submit_enable_check() {
        var enable_submit = username_valid && email_valid && password_valid
        $("#register_submit").prop("disabled", !enable_submit)
    }

    $("#id_username").change(function() {
        var input = $(this)
        username = $(this).val();
 
        $.ajax({
            url : "/validate_username/",
            data: {
                'username': username
            },
            dataType: "json",
            success: function(data) {
                var username_is_valid = (data.error_message).length == 0;
                username_valid = username_is_valid && username.length
                if (!username_is_valid) {
                    var error_message = data.error_message;
                }
                $("#error_1_id_username").remove()
                if (!username_is_valid) {
                    $("#id_username").css("border", "2px solid #dc3545");
                    var error_feedback = $( '<p id="error_1_id_username" style="display:block" class="invalid-feedback"><strong>'+ error_message +'</strong></p>' );
                    error_feedback.insertAfter(input);
                } else {
                    $("#id_username").css("border", "2px solid #00bfa6")
                }
                submit_enable_check()
            }
        });
    });

    $("#id_email").change(function() {
        var input = $(this)
        var email = input.val()
        $.ajax({
            url : "/validate_email/",
            data : {
                'email' : email
            },
            dataType : 'json',
            success : function(data) {
                var error_message = data.error_message
                email_valid = (error_message.length == 0) && email.length
                $("#error_1_id_email").remove()
                if (error_message.length) {
                    input.css("border", "2px solid #dc3545")
                    var error_feedback = $('<p id="error_1_id_email" style="display:block" class="invalid-feedback"><strong>'+ error_message +'</strong></p>')
                    error_feedback.insertAfter(input)
                    email_valid = false
                } else {
                    input.css("border", "2px solid #00bfa6")
                    email_valid = true
                }
                submit_enable_check()
            }
            


        })
    });

    $("#id_password1, #id_password2").keyup(function(){
        // var input = $(this)
        var password1 = $("#id_password1").val()
        var password2 = $("#id_password2").val()

        var input = (password1.length) ? $("#id_password1") :  $("#id_password2")

        $.ajax({
            url : "/validate_password/",
            data: {
                'password1' : password1,
                'password2' : password2,
            },
            dataType: "json",
            success: function(data) {
                var error_message = data.error_message
                password_valid = (!error_message.length && password1.length && password2.length)
                var error_feedback = $( '<p id="error_1_id_password1" style="display:block" class="invalid-feedback"><strong>'+ error_message +'</strong></p>' );
                $("#error_1_id_password1").remove()
                $("#error_1_id_password2").remove()
                if (error_message == "The two password fields didn’t match.") {
                    $("#id_password1").css("border", "2px solid #dc3545");
                    $("#id_password2").css("border", "2px solid #dc3545");
                    error_feedback.insertAfter(input)
                } else if (error_message == "") {
                    $("#id_password1").css("border", "2px solid #00bfa6")
                    $("#id_password2").css("border", "2px solid #00bfa6")
                } else {
                    input.css("border", "2px solid #dc3545")
                    error_feedback.insertAfter(input)
                }
                submit_enable_check()
            }
        });
    });

    $("#register-form").submit(function(event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/register/',
            dataType: 'json',
            data: {
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
                username : $("#id_username").val(),
                email : $("#id_email").val(),
                password1 : $("#id_password1").val(),
                password2 : $("#id_password2").val(),
                isTenagaMedis : ($("#id_isTenagaMedis").prop('checked')) ? 'on' : '',
            },
            success: function() {
                success_message = $('<p class="invalid-feedback" id="register-success" ><strong> Akun berhasil dibuat, silahkan login</strong></p>')
                $('#register-div').hide();
                $('#login-div').show();
                success_message.insertBefore($('#username-input'))
            }

        })
    })
});