from django.test import LiveServerTestCase, TestCase, tag, RequestFactory
from django.urls import reverse
from .models import Profile
from django.contrib.auth.models import User
from main.views import validate_username, passwordValidation, email_validation
import json


class MainTestCase(TestCase):
    def setUp(self):
        self.request = RequestFactory()
        self.user = User.objects.create_user(username='username', password='password', email="user@mail.com")
        self.user.profile = Profile.objects.create(user=self.user, isTenagaMedis=True)

    #home
    def test_url_home_status_200(self):
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = self.client.get(reverse('main:home'))
        self.assertTemplateUsed(response, "main/home.html")

    def test_content_home(self):
        response = self.client.get(reverse('main:home'))
        isi_template = response.content.decode('utf8')

        self.assertIn("SeputarCOVID", isi_template)
        self.assertIn("portal aplikasi seputar COVID-19", isi_template)
        self.assertIn("Daftar",isi_template)
        self.assertIn("Login", isi_template)
    

    #register
    def test_user_exist_after_registration(self):
        datauser = {
            'username': 'usertest',
            'email' : 'usertest@company.com',
            'password1': 'testingtesting123',
            'password2': 'testingtesting123',
            'isTenagaMedis' : True
        }
        response = self.client.post(reverse('main:register'), datauser)
        usertest = User.objects.get(username= "usertest")
        self.assertEqual(usertest.username, 'usertest')
        self.assertEqual(usertest.email, 'usertest@company.com')
        self.assertEqual(usertest.profile.isTenagaMedis, True)

    def test_validate_username(self):
        request1 = self.request.get("/validate_username/?username=validusername")   # valid username
        request2 = self.request.get("/validate_username/?username=username")        # taken username
        request3 = self.request.get("/validate_username/?username=????")            # invalid username

        response1 = validate_username(request1)
        response2 = validate_username(request2)
        response3 = validate_username(request3)

        data1 = json.loads(response1.content)
        data2 = json.loads(response2.content)
        data3 = json.loads(response3.content)

        self.assertEqual(data1["error_message"], "")
        self.assertEqual(data2["error_message"], "A user with that username already exists.")
        self.assertEqual(data3["error_message"], "Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters.")

    def test_validate_password(self):
        request1 = self.request.get("/validate_password/?password1=passwordone&password2=passwordtwo")  # non matching password
        request2 = self.request.get("/validate_password/?password1=short&password2=short")              # too short password
        request3 = self.request.get("/validate_password/?password1=password&password2=password")        # too common password
        request4 = self.request.get("/validate_password/?password1=1470235689&password2=1470235689")    # all numeric password

        response1 = passwordValidation(request1)
        response2 = passwordValidation(request2)
        response3 = passwordValidation(request3)
        response4 = passwordValidation(request4)

        data1 = json.loads(response1.content)
        data2 = json.loads(response2.content)
        data3 = json.loads(response3.content)
        data4 = json.loads(response4.content)

        self.assertEqual(data1["error_message"], "The two password fields didn’t match.")
        self.assertEqual(data2["error_message"], "This password is too short. It must contain at least 8 characters.")
        self.assertEqual(data3["error_message"], "This password is too common.")
        self.assertEqual(data4["error_message"], "This password is entirely numeric.")

    def test_validate_email(self):
        request1 = self.request.get("/validate_email/?email=validemail@mail.com")   # valid email
        request2 = self.request.get("/validate_email/?email=invalidemail@")         # invalid email

        response1 = email_validation(request1)
        response2 = email_validation(request2)

        data1 = json.loads(response1.content)
        data2 = json.loads(response2.content)

        self.assertEqual(data1["error_message"], "")
        self.assertEqual(data2["error_message"], "Enter a valid email address.")



    #login
    def test_url_login(self):
        response = self.client.get(reverse('main:login'))
        self.assertEqual(response.status_code, 200)
    
    def test_template_login(self):
        response = self.client.get(reverse('main:login'))
        self.assertTemplateUsed(response, "main/home.html")

    def test_content_login(self):
        response = self.client.get(reverse('main:login'))
        isi_template = response.content.decode('utf8')

        self.assertIn("Login", isi_template)
        self.assertIn("Username", isi_template)
        self.assertIn("Password", isi_template)
        self.assertIn("Belum punya akun?", isi_template)
        self.assertIn("Daftar", isi_template)
    
    def test_user_is_authenticated_after_login(self):
        data = {
            'username' : 'username',
            'password' : 'password'
        }
        response = self.client.post(reverse('main:login'), data=data)
        self.assertTrue(self.user.is_authenticated)

    #logout
    def test_url_logout(self):
        response = self.client.get(reverse('main:logout'))
        self.assertEqual(response.status_code, 302)
