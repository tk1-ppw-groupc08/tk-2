from django.urls import path
from django.contrib.auth import views as auth_views


from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('register/',views.register, name='register'),
    path('login/',views.login_view, name='login'),
    path('logout/',views.logout_view, name='logout'),
    path('validate_username/', views.validate_username, name = "validate_username"),
    path('validate_password/', views.passwordValidation, name = "validate_password"),
    path('validate_email/', views.email_validation, name = "validate_email")
]
