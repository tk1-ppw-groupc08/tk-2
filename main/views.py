import re
from django.http import JsonResponse
from django.urls import resolve
from django.shortcuts import render, redirect
from django.core.validators import validate_email
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.contrib import messages
from .forms import UserRegistrationForm
from .models import Profile

def home(request):
    if request.method == 'POST':
        login_view(request)
    form = UserRegistrationForm()
    return render(request, 'main/home.html', {'form':form})

def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return_url = request.GET.get('next')
            return redirect(return_url or 'main:home')
        else :
            messages.info(request, "Incorrect username or password")
    
    return render(request, 'main/home.html')

def logout_view(request):
    logout(request)
    return redirect('main:home')

def register(request):
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            isTenagaMedis = form.cleaned_data.get('isTenagaMedis')
            Profile.objects.create(user=user, isTenagaMedis=isTenagaMedis)
            data = {}
            return JsonResponse(data)

def validate_username(request):
    username = request.GET.get("username", None)
    data = {}
    if (User.objects.filter(username__iexact=username).exists()):
        data["error_message"] = "A user with that username already exists."
    elif (not re.match(r'^$|^[\w.@+-]+\Z', username)):
        data["error_message"] = "Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters."
    else:
        data["error_message"] = ""
    return JsonResponse(data)

def email_validation(request):
    email = request.GET.get("email", None)
    data = { 'error_message' : ''}
    try:
        validate_email(email)
    except ValidationError as e:
        data['error_message'] = str(e.messages[0])
    return JsonResponse(data)

def passwordValidation(request):
    password1 = request.GET.get("password1", None)
    password2 = request.GET.get("password2", None)
    data = {
        'error_message' : ''
    }
    if password1 and password2 and password1 != password2:
        data["error_message"] = "The two password fields didn’t match."
    elif password1 or password2:
        try:
            if (password1):
                validate_password(password1)
            else:
                validate_password(password2)
        except ValidationError as e:
            data["error_message"] = str(e.messages[0])
    return JsonResponse(data)

