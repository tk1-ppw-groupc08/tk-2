[![pipeline status](https://gitlab.com/tk1-ppw-groupc08/tk-2/badges/master/pipeline.svg)](https://gitlab.com/tk1-ppw-groupc08/tk-2/-/commits/master)
[![coverage report](https://gitlab.com/tk1-ppw-groupc08/tk-2/badges/master/coverage.svg)](https://gitlab.com/tk1-ppw-groupc08/tk-2/-/commits/master)

# Anggota Group :
- 1906353422	Nathania Calista
- 1906351064	Ryanda Mahdy Ananda
- 1906350906	Zaki Indra Yudhistira
- 1906350843	Jafar Abdurrohman
- 1906350963	Muhammad Hanif Anggawi

# Link Herokuapp:
http://seputarcovid.herokuapp.com/

# Deskripsi:
SeputarCOVID adalah portal aplikasi web mengenai informasi terkini mengenai COVID-19 dan bagaimana cara menghadapi pandemi ini mulai dari diri sendiri. SeputarCOVID bertujuan untuk memberikan informasi terkini dan faktual seputar COVID-19 melalui artikel, konsultasi tanya jawab oleh tenaga medis yang berpengalaman, fitur lapor diri untuk membantu memantau penyebaran COVID19, dan fitur Donasi untuk memberi donasi kepada tenaga medis.

# Daftar Fitur
1. Login/Register
2. Artikel
3. Forum Konsultasi
4. Lapor diri
5. Donasi 