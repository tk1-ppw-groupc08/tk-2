$("#root").ready(function() {
    const url = "/forum/request/";
    $.ajax({
        url: url,
        success: function(response) {
            const tenagaMedisPic = "/static/img/tenaga_medis.svg";
            const normalPic = "/static/img/default.svg";
            const threads = $("#root > .threads");
            for (let i = 0; i < response.length; i++) {
                const id = response[i].id;
                const username = response[i].username;
                const isTenagaMedis = response[i].isTenagaMedis;
                const datePosted = response[i].date_posted;
                const topic = response[i].topic;
                var string;
                if (isTenagaMedis) {
                    string = `            
                        <div class="thread-item">
                            <div class="profile-img">
                                <img src=${tenagaMedisPic} alt="">
                            </div>
                            <div class="thread-desc">
                                <div class="topic">
                                    <a href="/forum/${id}/">${topic}</a>
                                </div>
                                <div class="t-id">
                                    <p style="display: inline; font-weight: 600;">${username}</p>
                                    <p style="display: inline; color:#00bfa6; font-weight: 600;">Tenaga Medis</p>
                                    <p style="display: inline; color: gray;"> Posted on ${datePosted}</p>
                                </div>
                            </div>
                            <div class="direct-answer">
                                <a href="${id}?login=1">Jawab ></a>
                            </div>
                        </div>`
                } else {
                    string = `            
                        <div class="thread-item">
                            <div class="profile-img">
                                <img src=${normalPic} alt="">
                            </div>
                            <div class="thread-desc">
                                <div class="topic">
                                    <a href="/forum/${id}/">${topic}</a>
                                </div>
                                <div class="t-id">
                                    <p style="display: inline; font-weight: 600;">${username}</p>
                                    <p style="display: inline; color: gray;"> Posted on ${datePosted}</p>
                                </div>
                            </div>
                            <div class="direct-answer">
                                <a href="${id}?login=1">Jawab ></a>
                            </div>
                        </div>`
                }
                threads.append(string);
            }
        }
    });
});

$("#searchbox").keyup(function() {
    const keyword = $("#searchbox").val();
    const url = `/forum/request?q=${keyword}`;
    $.ajax({
        url: url,
        success: function(response) {
            $("#root > .threads").html("");
            const root = $("#root");
            const tenagaMedisPic = "/static/img/tenaga_medis.svg";
            const normalPic = "/static/img/default.svg";
            const threads = root.children(".threads");
            for (let i = 0; i < response.length; i++) {
                const id = response[i].id;
                const username = response[i].username;
                const isTenagaMedis = response[i].isTenagaMedis;
                const datePosted = response[i].date_posted;
                const topic = response[i].topic;
                var string;
                if (isTenagaMedis) {
                    string = `            
                        <div class="thread-item">
                            <div class="profile-img">
                                <img src=${tenagaMedisPic} alt="">
                            </div>
                            <div class="thread-desc">
                                <div class="topic">
                                    <a href="/forum/${id}/">${topic}</a>
                                </div>
                                <div class="t-id">
                                    <p style="display: inline; font-weight: 600;">${username}</p>
                                    <p style="display: inline; color:#00bfa6; font-weight: 600;">Tenaga Medis</p>
                                    <p style="display: inline; color: gray;"> Posted on ${datePosted}</p>
                                </div>
                            </div>
                            <div class="direct-answer">
                                <a href="${id}?login=1">Jawab ></a>
                            </div>
                        </div>`
                } else {
                    string = `            
                        <div class="thread-item">
                            <div class="profile-img">
                                <img src=${normalPic} alt="">
                            </div>
                            <div class="thread-desc">
                                <div class="topic">
                                    <a href="/forum/${id}/">${topic}</a>
                                </div>
                                <div class="t-id">
                                    <p style="display: inline; font-weight: 600;">${username}</p>
                                    <p style="display: inline; color: gray;"> Posted on ${datePosted}</p>
                                </div>
                            </div>
                            <div class="direct-answer">
                                <a href="${id}?login=1">Jawab ></a>
                            </div>
                        </div>`
                }
                threads.append(string);
            }
        }
    });
});

$(".replies").on("click", ".cancel-button", function() {
    $(this).closest(".reply-content").hide();
    $(".reply").show();
    $(".bottom-button").show();
});

$(".head-thread").on("click", ".newreply", function() {
    $(".reply-content").hide();
    $(".reply").show();
    $(".replies").children(".reply-content:first-child").css("display", "flex");
    $(".bottom-button").hide();
});

$(".replies").on("click", ".edit-button", function() {
    const item = $(this).closest(".reply");
    const content = $(this).parent().siblings(".content").children("p").html();
    item.next().find(".text-content").html(content);
    $(".reply").show();
    item.css("display", "none");
    $(".reply-content").css("display", "none");
    item.next().css("display", "flex");
    $(".bottom-button").hide();
});

$(".replies").on("click", ".delete-reply", function() {
    const replyId = $(this).data("id");
    $.ajax({
        headers: { "X-CSRFToken": Cookies.get('csrftoken')},
        url: `${window.location.href}${replyId}/delete`,
    })
    .done(data => {
        if (data) {
            const reply = $(this).closest(".reply");
            reply.next().remove();
            reply.remove();
        }
    });
});

$(".replies").on("submit", ".edit-form", function(e) {    
    e.preventDefault();
    $(".bottom-button").show();
    const replyId = $(this).find(".button-fillet").data("id");
    const content = $(this).children("textarea").val();
    const formData = {
            thread_id : parseInt(window.location.href.split("/")[4], 10),
            content : content,
        };
    $.ajax({
        headers: { "X-CSRFToken": Cookies.get('csrftoken')},
        url: `${window.location.href.replace('?login=1','')}${replyId}/edit/submit/`,
        type: "POST",
        data: formData,
        dataType: "json",
    })
    .done(data => {
        if (data.success) {
            $(this).closest(".reply-content").find(".text-content").html(content);
            const pair = $(this).closest(".reply-content").prev();
            pair.show();
            pair.find(".content").children("p").html(content);
            pair.find(".t-id").children("p:last-child").css({"display": "inline", "color": "gray"});
            $(this).closest(".reply-content").hide();
        }
    });
});

$(".replies").on("submit", ".new-form", function(e) {
    e.preventDefault();
    const divReplies = $(".replies > .reply-content:first-child");
    const tenagaMedisPic = "/static/img/tenaga_medis.svg";
    const normalPic = "/static/img/default.svg";
    const formData = {
            thread_id : parseInt(window.location.href.split("/")[4], 10),
            content : $(this).children("textarea").val(),
        };
    $.ajax({
        headers: { "X-CSRFToken": Cookies.get('csrftoken')},
        url: `${window.location.href.replace('?login=1','')}reply/submit/`,
        type: "POST",
        data: formData,
        dataType: "json",
    })
    .done(data => {
        $(".reply-button").parent().show();
        if (data.success) {
            $(".replies > .reply-content:first-child textarea").val("");
            $(".replies").children(".reply-content:first-child").hide();
            const replyId = data.rId;
            const username = data.username;
            const isTenagaMedis = data.isTenagaMedis;
            const datePosted = data.date_posted;
            const content = data.content;
            const mainTopic = data.main_topic;

            if (isTenagaMedis) {
                string = `
                    <div class="reply">
                        <div class="reply-img">
                            <img src=${tenagaMedisPic} alt="">
                        </div>
                        <div class="topic-content">
                            <div class="topic-bundle">
                                <div class="topic">
                                    <span>Re: ${mainTopic}</span>
                                </div>
                                <div class="t-id">
                                    <p style="display: inline; color:#00bfa6; font-weight: 600;">${username}</p>
                                    <p style="display: inline; color: gray;"> Posted on ${datePosted}</p>
                                    <p style="display: none; color: gray;"> (edited)</p>
                                </div>
                            </div>
                            <div class="content">
                                <p>${content}</p>
                            </div>
                            <div class="bottom-button">
                                <a class="reply-button edit-button" data-id="${replyId}">Edit</a>
                                <a class="delete delete-reply" data-id="${replyId}">Hapus</a>
                            </div>
                        </div>
                    </div> 
                    <div class="reply-content editreply" style="display:none;">
                        <div class="reply-img addreply">
                            <img src=${tenagaMedisPic} alt="">
                        </div>
                        <div class="topic-content">
                            <div class="topic-bundle">
                                <div class="t-id">
                                    <p style="display: inline; color:#00bfa6; font-weight: 600;">${username}</p>
                                </div>
                                <div class="topic">
                                    <span>Reply: ${mainTopic}</span>
                                </div>
                            </div>
                            <div class="text-reply">
                                <form class="edit-form" action="submit/" method="POST">
                                    <textarea class="text-content" name="content" cols="30" rows="10" placeholder="Tulis jawabanmu" style="padding: 10px;" required></textarea>
                                    <div id="button">
                                        <a class="button-outlined cancel-button">Cancel</a>
                                        <input class="button-fillet" type="submit" value="Edit" data-id="${replyId}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    `
            } else {
                string = `
                    <div class="reply">
                        <div class="reply-img">
                            <img src=${normalPic} alt="">
                        </div>
                        <div class="topic-content">
                            <div class="topic-bundle">
                                <div class="topic">
                                    <span>Re: ${mainTopic}</span>
                                </div>
                                <div class="t-id">
                                    <p style="display: inline; font-weight: 600;">${username}</p>
                                    <p style="display: inline; color: gray;"> Posted on ${datePosted}</p>
                                    <p style="display: none; color: gray;"> (edited)</p>
                                </div>
                            </div>
                            <div class="content">
                                <p>${content}</p>
                            </div>
                            <div class="bottom-button">
                                <a class="reply-button edit-button" data-id="${replyId}">Edit</a>
                                <a class="delete delete-reply" data-id="${replyId}">Hapus</a>
                            </div>
                        </div>
                    </div>
                    <div class="reply-content editreply" style="display:none;">
                        <div class="reply-img addreply">
                            <img src=${normalPic} alt="">
                        </div>
                        <div class="topic-content">
                            <div class="topic-bundle">
                                <div class="t-id">
                                    <p style="display: inline; font-weight: 600;">${username}</p>
                                </div>
                                <div class="topic">
                                    <span>Reply: ${mainTopic}</span>
                                </div>
                            </div>
                            <div class="text-reply">
                                <form class="edit-form" action="submit/" method="POST">
                                    <textarea class="text-content" name="content" cols="30" rows="10" placeholder="Tulis jawabanmu" style="padding: 10px;" required></textarea>
                                    <div id="button">
                                        <a class="button-outlined cancel-button">Cancel</a>
                                        <input class="button-fillet" type="submit" value="Edit" data-id="${replyId}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    `
            }
        } 
        divReplies.after(string);
    });
});
