from django.urls import path
from .views import *

app_name = 'forum'

urlpatterns = [
    path('', root, name="root"),
    path('request/', topic_request, name="topic_request"),
    path('add/', add_thread, name="add new thread"),
    path('add/submit/', submit_thread, name="submit new thread"),
    path('<int:thread_id>/', thread, name="thread"),
    path('<int:thread_id>/delete/', delete_thread, name="delete thread"),
    path('<int:thread_id>/reply/submit/',
         submit_reply, name="submit reply"),
    path('<int:thread_id>/<int:reply_id>/delete/',
         delete_reply, name="delete reply"),
    path('<int:thread_id>/<int:reply_id>/edit/submit/',
         submit_edit, name="submit edit reply")
]