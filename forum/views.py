from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.contrib.auth.models import User
from django.core import serializers
from .models import Thread, ThreadReply, ThreadForm, ThreadReplyForm
from datetime import datetime
from django.views.decorators.csrf import csrf_protect
import json


def root(request):
    """
    basic forum views
    """
    return render(request, 'forum/root.html')

def topic_request(request):
    try:
        keyword = request.GET['q']
        threads = Thread.objects.filter(topic__icontains=keyword).order_by("-date_posted")
    except KeyError as w:
        threads = Thread.objects.all().order_by("-date_posted")

    threads = [{'id': thread.id, 'username': thread.user.username, 'isTenagaMedis': thread.user.profile.isTenagaMedis, 'topic': thread.topic,\
        'date_posted': format_date(thread.date_posted)} for thread in threads]

    return JsonResponse(threads, safe=False)

@login_required
def add_thread(request):
    """
    form views for adding new thread **login required
    """
    form = ThreadForm()
    context = {
        'form' : form
    }
    return render(request, 'forum/add_thread.html', context)

@login_required
def submit_thread(request):
    """
    views for new thread submitting form **login required
    """
    if request.method == "POST":
        form = ThreadForm(request.POST)
        if form.is_valid():
            form.instance.user = request.user
            form.save()
    
    return redirect('forum:root')

def thread(request, thread_id):
    """
    views thread with specific topic
    """
    try:
        val = request.GET['login']
    except KeyError:
        val = None

    if val == "1" and not request.user.is_authenticated:
        return redirect(f"/login/?next=/forum/{thread_id}/")

    thread = Thread.objects.get(id=thread_id)
    thread.date_posted = format_date(thread.date_posted)
    replies = ThreadReply.objects.filter(main_thread=thread).all().order_by("-date_posted")
    replies = [format_attr(reply) for reply in replies]
    context = {
        'thread' : thread,
        'replies' : replies
    }
    return render(request, 'forum/topic.html', context)


@login_required
def delete_thread(request, thread_id):
    """
    delete thread (only if current user login equal to thread author)
    """
    thread = Thread.objects.get(id=thread_id)
    if request.user == thread.user:
        thread.delete()
    return redirect('forum:root')


@login_required
def submit_reply(request, thread_id):
    """
    views for submitting form
    """
    new_reply = {'success': False}
    if request.method == "POST":
        form = ThreadReplyForm(request.POST)
        if form.is_valid():
            thread = Thread.objects.get(id=thread_id)
            form.instance.main_thread = thread
            form.instance.user = request.user
            now = datetime.now()
            reply = form.save()
            username = request.user.username
            isTenagaMedis = request.user.profile.isTenagaMedis
            date_posted = format_date(now)
            content = reply.content
            topic = reply.main_thread.topic
            new_reply = {'success': True, 'rId': reply.id, 'username': username, 'main_topic' : topic,\
                'isTenagaMedis': isTenagaMedis, 'date_posted': date_posted, 'content': content}

    return JsonResponse(new_reply, safe=False)


@login_required
def delete_reply(request, thread_id, reply_id):
    """
    views for delete reply
    """
    reply = ThreadReply.objects.get(id=reply_id)
    if request.user == reply.user:
        reply.delete()
        return JsonResponse(True, safe=False)
    return JsonResponse(False, safe=False)

@login_required
def submit_edit(request, thread_id, reply_id):
    """
    submit edited reply
    """
    if request.method == "POST":
        form = ThreadReplyForm(request.POST)
        reply = ThreadReply.objects.get(id=reply_id)
        if form.is_valid() and request.user == reply.user:
            reply.content = form.instance.content
            reply.is_edited = True
            reply.save()
            return JsonResponse({'success': True}, safe=False)

    return JsonResponse({'success': False}, safe=False)


# Helper Method

def format_date(date):
    string = date.strftime("%b %d, %Y, at %H:%M")
    return string

def format_attr(datetime_obj):
    datetime_obj.date_posted = format_date(datetime_obj.date_posted)
    return datetime_obj