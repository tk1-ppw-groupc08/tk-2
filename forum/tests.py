from django.test import TestCase, Client, RequestFactory
from django.urls import resolve, reverse
from .views import *
from .models import Thread, ThreadReply, ThreadForm, ThreadReplyForm
from main.models import Profile
from django.contrib.auth.models import User, AnonymousUser
from django.utils import timezone, timesince


""" *snippet
self.user = User.objects.create_user(username='testuser', password='12345')
login = self.client.login(username='testuser', password='12345')
"""
class TestForumApp(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='jacob', email='jacob@xd.com', password='top_secret')
        self.user.save()
        self.profile = Profile.objects.create(user=self.user, isTenagaMedis=True)
        self.profile.save()
        self.thread = Thread(topic="abc", content="abc", user=self.user)
        self.thread.save()
        self.reply = ThreadReply.objects.create(
            content="xdxd", user=self.user, main_thread=self.thread)
        self.reply.save()
        self.user = User.objects.create_user(
            username='bee', email='bee@xd.com', password='top_secret')
        self.user.save()
        self.profile = Profile.objects.create(
            user=self.user, isTenagaMedis=False)
        self.profile.save()
    

    """
    root **no login
    """
    def test_root_is_exist(self):
        response = Client().get('/forum/')
        self.assertEqual(response.status_code, 200)

    def test_root_views(self):
        found = resolve('/forum/')
        self.assertEqual(found.func, root)

    def test_root_use_template(self):
        response = Client().get('/forum/')
        self.assertTemplateUsed(response, 'forum/root.html')


    """
    ajax topic request **no login
    """
    def test_topic_request_is_exist(self):
        response = Client().get(f"{reverse('forum:topic_request')}")
        self.assertEqual(response.status_code, 200)
        response = Client().get(f"{reverse('forum:topic_request')}?q=hhdshtabf")
        self.assertEqual(response.status_code, 200)

    def test_topic_request_views(self):
        found = resolve(reverse('forum:topic_request'))
        self.assertEqual(found.func, topic_request)


    """
    thread **no login
    """
    def test_thread_is_exist(self):
        response = Client().get(f'/forum/{self.thread.id}/')
        self.assertEqual(response.status_code, 200)
        response = Client().get(f'/forum/{self.thread.id}/', {'login': 1})
        self.assertEqual(response.status_code, 302)

    def test_thread_views(self):
        found = resolve(f'/forum/{self.thread.id}/')
        self.assertEqual(found.func, thread)

    def test_thread_use_template(self):
        response = Client().get(f'/forum/{self.thread.id}/')
        self.assertTemplateUsed(response, 'forum/topic.html')

    
    """
    add thread **login required
    """
    def test_add_with_login(self):
        request = self.factory.get('/forum/add/')
        request.user = self.user
        response = add_thread(request)
        self.assertEqual(response.status_code, 200)

    def test_add_without_login(self):
        request = self.factory.get('/forum/add/')
        request.user = AnonymousUser()
        response = add_thread(request)
        self.assertEqual(response.status_code, 302)

    def test_add_views(self):
        found = resolve('/forum/add/')
        self.assertEqual(found.func, add_thread)

    def test_add_use_template(self):
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.get('/forum/add/')
        self.assertTemplateUsed(response, 'forum/add_thread.html')


    """
    submit thread **login required
    """
    def test_submit_thread_with_login(self):
        data = {
            'topic' : 'rearea',
            'content' : 'rearea'
        }
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post('/forum/add/submit/', data=data)
        thread = Thread.objects.get(topic="rearea")
        self.assertEqual(response.status_code, 302)
        self.assertEqual(thread.user.username, "jacob")
        self.assertEqual(thread.content, "rearea")


    """
    delete thread **login required
    """
    def test_delete_thread_with_login(self):
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post('/forum/1/delete/')
        self.assertEqual(response.status_code, 302)
    
    
    """
    submit reply **login required
    """
    def test_submit_reply_with_login(self):
        data = {
            'content' : 'rearea'
        }
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post(f'/forum/{self.thread.id}/reply/submit/', data=data)
        reply = ThreadReply.objects.get(id=2)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(reply.user.username, "jacob")
        self.assertEqual(reply.content, "rearea")

        self.assertEqual(response.json()['success'], True)
        self.assertEqual(response.json()['rId'], reply.id)
        self.assertEqual(response.json()['username'], 'jacob')


    """
    delete reply **login required
    """
    def test_delete_reply_with_login(self):
        reply = ThreadReply(content="rearea", user=self.user, main_thread=self.thread)
        reply.save()
        c = Client()
        c.login(username='bee', password='top_secret')
        response = c.get('/forum/1/1/delete/')
        self.assertEqual(response.json(), False)
        c.logout()
        c.login(username='jacob', password='top_secret')
        response = c.get('/forum/1/1/delete/')
        self.assertEqual(response.json(), True)


    """
    submit edit reply **login required
    """
    def test_submit_edit_with_login(self):
        data = {
            'content' : 'rearea'
        }
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post(f'/forum/{self.thread.id}/{self.reply.id}/edit/submit/', data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['success'], True)
        c.logout()
        c.login(username='bee', password='top_secret')
        response = c.post(
            f'/forum/{self.thread.id}/{self.reply.id}/edit/submit/', data=data)
        self.assertEqual(response.json()['success'], False)


class TestModel(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='jacob', email='jacob@xd.com', password='top_secret')
        self.thread = Thread(topic="abc", content="abc", user=self.user)
        self.thread.save()

    def test_thread_new_object(self):
        self.assertEqual(Thread.objects.all().count(), 1)

    def test_reply_new_object(self):
        reply = ThreadReply.objects.create(content="xdxd", user=self.user, main_thread=self.thread)
        reply.save()
        self.assertEqual(Thread.objects.all().count(), 1)
