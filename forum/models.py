from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.utils import timezone
from datetime import datetime

class Thread(models.Model):
    topic = models.CharField(max_length=100)
    content = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_posted = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)
    is_edited = models.BooleanField(default=False)

    def __str__(self):
        return self.topic

class ThreadReply(models.Model):
    content = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_posted = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)
    main_thread = models.ForeignKey(Thread, on_delete=models.CASCADE, related_name="reply")
    is_edited = models.BooleanField(default=False)

    def __str__(self):
        return self.content

class ThreadForm(ModelForm):
    class Meta:
        model = Thread
        fields = ['topic', 'content']

class ThreadReplyForm(ModelForm):
    class Meta:
        model = ThreadReply
        fields = ['content']