from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from http import HTTPStatus
from .views import *
from .models import *
from .apps import LapordiriConfig

# Create your tests here.
class LaporDiriUnitTest(TestCase):

    def test_lapordiri_url_exist(self):
        response = Client().get('/lapordiri/')
        self.assertEqual(response.status_code, 200)

    def test_lapordiri_using_index_func(self):
        found = resolve('/lapordiri/')
        self.assertEqual(found.func, lapordiri_home)
    
    def test_lapordiri_using_template(self):
        response = Client().get('/lapordiri/')
        self.assertTemplateUsed(response, 'home.html')

    def test_lapordiri_lapor_url_exist(self):
        response = Client().get('/lapordiri/lapor')
        self.assertEqual(response.status_code, 200)

    def test_lapordiri_lapor_using_index_func(self):
        found = resolve('/lapordiri/lapor')
        self.assertEqual(found.func, lapordiri_lapor)
    
    def test_lapordiri_lapor_using_template(self):
        response = Client().get('/lapordiri/lapor')
        self.assertTemplateUsed(response, 'lapor.html')

    def test_lapordiri_Laporan_new_instance(self):
        new = Laporan.objects.create(kota='Exkota')
        count = Laporan.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(new.kota,'Exkota')
        self.assertEqual(new.jumlahPos,1)
        self.assertEqual(str(new),'Exkota')

    def test_lapordiri_POST_success(self):
        response = self.client.post('/lapordiri/lapor', 
            data = {"nama": "asd", "kota": 'Bruhkota', "telp": '12341', "prov": 'Demprov'})
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response["Location"], "/lapordiri/")
        new_res = self.client.get('/lapordiri/')
        html_response = new_res.content.decode('utf8')
        self.assertIn('Bruhkota', html_response)

    def test_lapordiri_POST_existing_instance(self):
        response = self.client.post('/lapordiri/lapor', 
            data = {"nama": "asd", "kota": 'Bruhkota', "telp": '12341', "prov": 'Demprov'})
        response = self.client.post('/lapordiri/lapor', 
            data = {"nama": "asde", "kota": 'Bruhkota', "telp": '2343', "prov": 'Demprov'})
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response["Location"], "/lapordiri/")
        new_res = self.client.get('/lapordiri/')
        html_response = new_res.content.decode('utf8')
        self.assertIn('Bruhkota', html_response)
        self.assertIn('2', html_response)
        count = Laporan.objects.all().count()
        self.assertEqual(count,1)

    def test_apps_lapordiri(self):
        self.assertEqual(LapordiriConfig.name, 'lapordiri')
        self.assertEqual(apps.get_app_config('lapordiri').name, 'lapordiri')
