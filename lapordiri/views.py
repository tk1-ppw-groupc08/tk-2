from django.shortcuts import render, redirect
from .forms import LaporForm
from .models import Laporan
from django.http.response import JsonResponse
from django.db.models import Sum
from django.contrib.auth.decorators import *
from django.core import serializers

context = {}
# Create your views here.
def lapordiri_home(request):
    context['laporan'] = Laporan.objects.all()
    return render(request, 'home.html', context)

def lapordiri_lapor(request):
    if request.method == "POST":
        form = LaporForm(request.POST)
        if form.is_valid():
            kota = form.cleaned_data.get('kota')
            lapor = Laporan.objects
            for laporans in lapor.all():
                if laporans.kota.lower() == kota.lower():
                    laporans.jumlahPos += 1
                    laporans.save()
                    return redirect('lapordiri:home')
            Laporan.objects.create(kota=kota.capitalize())
            return redirect('lapordiri:home')
    else:
        form = LaporForm()
    return render(request, 'lapor.html', {'form': form})

def lapordiri_datatabel(request):
    tabel = list(Laporan.objects.values())
    return JsonResponse(tabel, safe=False)

