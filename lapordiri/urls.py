from django.urls import path
from django.contrib.auth import views as auth_views


from . import views

app_name = 'lapordiri'

urlpatterns = [
    path('', views.lapordiri_home, name='home'),
    path('lapor', views.lapordiri_lapor, name='lapor'),
    path('datatabel', views.lapordiri_datatabel, name='datatabel')
]