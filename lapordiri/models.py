from django.db import models

# Create your models here.
class Laporan(models.Model):
    kota = models.TextField(max_length=300)
    jumlahPos = models.IntegerField(default=1)

    def __str__ (self):
        return self.kota