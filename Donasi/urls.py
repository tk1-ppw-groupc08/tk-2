from django.urls import path
from .views import donasi, tambah_donasi, data_donasi, data_form

app_name = "Donasi"

urlpatterns = [
    path('', donasi, name="donasi"),
    path('form/', tambah_donasi, name='form'),
    path('data/', data_donasi, name='data'),
    path('form/data/', data_form, name='dataForm')
]