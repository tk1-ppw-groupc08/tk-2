from django.test import Client, TestCase, RequestFactory
from django.urls import resolve 
from .models import Article
from main.models import Profile
from .forms import ArticleForm
from django.contrib.auth.models import User, AnonymousUser
from .views import * 
import json

class TestArticle(TestCase):

    def setUp(self):
        # sets up the user and the object
        self.factory = RequestFactory()
        self.user = User.objects.create(username='nia', email='nia@gmail.com', password='12345678')
        self.profile = Profile.objects.create(user = self.user, isTenagaMedis='False')
        self.article = Article(title="123", img="", content="12345678", author=self.user)
        self.article.save()

    ### home/landing (no login) ###

    def test_home_url_exists(self):
        response = Client().get('/artikel/')
        self.assertEqual(response.status_code, 200)

    def test_home_func_is_used(self):
        response = Client().get('/artikel/')
        found = resolve('/artikel/')
        self.assertEqual(found.func, home)
    
    def test_home_template_is_used(self):
        response = Client().get('/artikel/')
        self.assertTemplateUsed(response, 'artikel/home.html')

    ### home data ####

    def test_home_data_url_exists(self):
        response = Client().get('/artikel/data/')
        self.assertEqual(response.status_code, 200)

    def test_home_data_func_is_used(self):
        response = resolve('/artikel/data/')
        self.assertEqual(response.func, home_data)


    ### details (no login) ###

    def test_details_url_exists(self):
        response = Client().get('/artikel/details/1/')
        self.assertEqual(response.status_code, 200)

    def test_details_func_is_used(self):
        response = Client().get('/artikel/details/1/')
        found = resolve('/artikel/details/1/')
        self.assertEqual(found.func, details)

    def test_details_template_is_used(self): 
        response = Client().get('/artikel/details/1/')
        self.assertTemplateUsed(response, 'artikel/details.html')

    ### details data ####

    def test_details_data_url_exists(self):
        response = Client().get('/artikel/details/1/data/')
        self.assertEqual(response.status_code, 200)

    def test_details_data_func_is_used(self):
        response = resolve('/artikel/details/1/data/')
        self.assertEqual(response.func, details_data)
    

    ### add (login) ###

    def test_add_url_exists_with_login(self):
        request = self.factory.get('/artikel/add/')
        request.user = self.user
        response = add(request)
        self.assertEqual(response.status_code, 200)

    def test_add_url_exists_without_login(self):
        request = self.factory.get('/artikel/add/')
        request.user = AnonymousUser()
        response = add(request)
        self.assertEqual(response.status_code, 302)

    def test_add_func_is_used(self):
        response = Client().get('/add/')
        found = resolve('/artikel/add/')
        self.assertEqual(found.func, add)

    def test_add_template_is_used(self): 
        self.client.force_login(self.user)
        response = self.client.get('/artikel/add/')
        self.assertTemplateUsed(response, 'artikel/add.html')

    def test_add_form(self):
        form_data = {
            'title' : "345",
            'img' : "",
            'content' : "abc", 
        }
        
        c = Client()
        c.login(username = 'nia', password = '12345678')
        response = c.post('/artikel/add/', data=form_data)
        self.assertTrue(response.status_code, 302)

    ### add data ####

    def test_add_data_func_is_used(self):
        response = resolve('/artikel/add/data/')
        self.assertEqual(response.func, add_data)

    def test_add_data_json(self):
        form_data = {
            'title' : "123",
            'img' : "",
            'content' : "abcdefghijkl",
        }

        self.client.force_login(self.user)
        response = self.client.post('/artikel/add/', data=form_data)

        response2 = self.client.post('/artikel/add/data/', data=form_data).content.decode('utf-8')
        response3 = json.loads(response2)
        self.assertEqual(response3['title'], '123')
        self.assertEqual(response3['img'], "")
        self.assertEqual(response3['content'], "abcdefghijkl")
        self.assertEqual(response3['author'], "nia")
        


    ### edit (login) ###
    
    def test_edit_url_exists(self): 
        c = Client()
        c.login(username = 'nia', password = '12345678')
        response = c.get('/artikel/edit/1/')
        self.assertEqual(response.status_code, 302)
   
    def test_edit_func_is_used(self):
        found = resolve('/artikel/edit/1/')
        self.assertEqual(found.func, edit)

    def test_edit_template_is_used(self):   
        self.client.force_login(self.user)
        response = self.client.get('/artikel/edit/1/')
        self.assertTemplateUsed(response, 'artikel/edit.html')

    def test_edit_post_method(self):
        form_data = {
            'title' : "123",
            'img' : "",
            'content' : "abcdefghijkl",
        }

        c = Client()
        c.login(username = 'nia', password = '12345678')
        response = c.post('/artikel/edit/1/', data=form_data)
        self.assertEqual(response.status_code, 302)

    ### edit data ###

    def test_edit_data_func_is_used(self):
        response = resolve('/artikel/edit/1/data/')
        self.assertEqual(response.func, edit_data)

    def test_edit_data_json(self):
        form_data = {
            'title' : "123",
            'img' : "",
            'content' : "abcdefghijkl",
        }
        
        c = Client()
        c.login(username = 'nia', password = '12345678')
        response = c.post('/artikel/edit/1/', data=form_data)
        response2 = c.post('/artikel/edit/1/data/', data=form_data).content.decode('utf-8')
        response3 = json.loads(response2)
      
        self.assertEqual(response3['title'], '123')
        self.assertEqual(response3['img'], "")
        self.assertEqual(response3['content'], "abcdefghijkl")


    # ### delete ###

    def test_delete_url_exists(self):
        c = Client()
        c.login(username = 'nia', password = '12345678')
        response = c.get('/artikel/delete/1/data/')
        self.assertEqual(response.status_code, 200)

    def test_delete_func_is_used(self):
        response = resolve('/artikel/delete/1/data/')
        self.assertEqual(response.func, delete_data)

        
    ### models ###

    def test_article_models(self):
        article = Article.objects.get(title="123")
        self.assertEqual("123", article.title)
        self.assertEqual("", article.img)
        self.assertEqual("12345678", article.content)
        self.assertEqual(self.user, article.author)

