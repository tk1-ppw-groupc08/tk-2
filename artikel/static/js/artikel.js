//display all items 
var temp = {}; 

function getAll(json) {
    $('#result').empty(); 
    var html = ""; 
    for (let i = json.article_arr.length - 1; i > -1 ; i--) {
        console.log("print" + i); 
        html += `<div class="card"><div class="row"><div class="col-md-4" style ="display:flex; align-items:center;" >`; 
        html += `<img class="d-flex w-100" src=` + json.article_arr[i].img + `width="100%" alt="" style="overflow:hidden;"> </div>`; 
        html += `<div class="col-md-8 px-3"> <div class="card-block px-3"> <br>`; 
        html += `<h2 class="card-title" style="font-weight:bold;">` + json.article_arr[i].title ; 
        html += `</h2><p class="text-muted">` + json.article_arr[i].time_posted.toString().substring(0,10) + " | " + json.article_arr[i].time_posted.toString().substring(11,16) + " WIB" + `</p>`; 
        
        html += `<div class="row mx-auto my-3">`
        if (json.article_arr[i].isTenagaMedis){
            html += `<img src="/static/images/tenaga_medis.svg" width="4%" alt="" style="margin-right:0.5rem;">
            <p style="margin-bottom:unset; color:#00BFA6">` + json.article_arr[i].author + `</p>`; 
        } else {
            html += `<img src="/static/images/default.svg" width="4%" alt="" style="margin-right:0.5rem;">
            <p style="margin-bottom:unset;">` + json.article_arr[i].author + `</p>`; 
        }
        
        html += `</div> <p class="card-text">`; 
        
        if (json.article_arr[i].content.length > 300) {
            html += json.article_arr[i].content.substring(0,300) + `...`; 
        } else {
            html += json.article_arr[i].content; 
        }
        
        html += `</p> <a href="` + '/artikel/details/' + json.article_arr[i].id + `" class="text-decoration-none" id="readmore">Read More</a>`; 

        if (json.article_arr[i].author == json.user.username) {
            html += ` <div class="text-right"> <a class="text-decoration-none" href="` + '/artikel/edit/'  + json.article_arr[i].id + `/" style="margin-right:0.5rem;">Edit Artikel</a>`;  
            html += `<a class="text-decoration-none" id="delete" href="/artikel/delete/`+ json.article_arr[i].id +`/data/" style="color:red;"> Hapus Artikel</a> </div>`; 
        }

        html += `<br><br></div></div></div></div>`; 
    }

    $('#result').append(html); 
}

$.ajax({
    type : 'GET', 
    url : '/artikel/data/', 
    success : function(json) {
        temp = json;  
        console.log("success"); 
        var html = "";  
        if (temp.article_arr.length > 0) {
            getAll(temp); 
        } else {
            $('#result').empty(); 
            html += `<div class="row2"><p>Belum ada artikel baru</p></div>`; 
            $('#result').append(html); 
        }
        
    }, 
}); 

$(document).ready(function(){
    console.log("ready"); 

    //jika gambar error maka direplace dengan placeholder 
    $("img").on("error", function() { 
        $(this).attr("src", "https://i0.wp.com/shahpourpouyan.com/wp-content/uploads/2018/10/orionthemes-placeholder-image-1.png?resize=1024%2C683&ssl=1"); 
    }); 

    $(".card").hover(function(){
        $(this).addClass("hover"); 
    }, function(){
        $(this).removeClass("hover"); 
    }); 
  
    $("#delete").click(function (event) {
        event.preventDefault(); 
        var href = $(this).attr('href'); 
        const url_splitted = href.split("/"); 

        if(confirm("Apakah Anda ingin menghapus artikel ini?")) {
            $.ajax({
                url : $(this).attr('href'), 
                success : function(){
                    $("#result").empty();  
                    console.log($(this).attr('href')); 
                    console.log(temp); 
                    for (let i = 0; i < temp.article_arr.length; i++) {
                       if(temp.article_arr[i].id == url_splitted[3]){
                            temp.article_arr.splice(i, 1); 
                       }

                    } 
                    
                    console.log(temp); 
                    getAll(temp); 
                }, 
                
            }); 

            alert("Artikel deleted");
            
        } else {
            event.preventDefault(); 
        }
    }); 
}); 

