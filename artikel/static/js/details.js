const url = window.location.pathname; 
console.log(url)
const url_splitted = url.split("/"); 
console.log(url_splitted)

$(document).ready( function() {
    $.ajax({
        type : 'GET', 
        url : `/artikel/details/` + url_splitted[3].toString() + `/data/`, 
        success : function(json) {
            console.log("success")
            title = ""; 
            title += json.title;
            $('#title').append(title); 

            time_posted = ""; 
            time_posted += json.time_posted.toString().substring(0,10) + " | " + json.time_posted.toString().substring(11,16) + " WIB"; 
            $('#time_posted').append(time_posted); 

            author = "";
            if(json.isTenagaMedis) {
                author += `<img src="/static/images/tenaga_medis.svg" id="profpic" alt="" style="margin-right:0.5rem;">`; 
                author += `<p style="margin-bottom:unset; color:#00BFA6">` + json.author +`</p>`; 
            } else {
                author += `<img src="/static/images/default.svg" id="profpic" alt="" style="margin-right:0.5rem;">`; 
                author += `<p style="margin-bottom:unset;">`+ json.author +`</p>`; 
            }
            //author += json.author;  
            $('#author').append(author); 

            img = ""; 
            img += `<img src="` + json.img + `" id="placeholder" alt="">`; 
            $('#img').append(img); 

            content = ""; 
            const content_split = json.content.split("\n"); 
            console.log(content_split); 
            //content += json.content; 
            for (let index = 0; index < content_split.length; index++) {
              content += content_split[index] + '<br>'; 
            }
            $('#content').append(content); 
        }
    }); 

    $("img").on("error", function() { 
        $(this).attr("src", "https://i0.wp.com/shahpourpouyan.com/wp-content/uploads/2018/10/orionthemes-placeholder-image-1.png?resize=1024%2C683&ssl=1"); 
    }); 

}); 