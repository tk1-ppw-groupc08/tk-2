from django import forms 
from .models import Article 
from django.forms import widgets, TextInput, Textarea, DateTimeInput, FileInput

class ArticleForm(forms.ModelForm):
    class Meta :
        model = Article
        fields = ['title', 'img', 'content']
        widgets = {
            'title' : TextInput(attrs={'placeholder' : 'Nama', 'id' : 'data-title', 'required' : True}), 
            'img' : TextInput(attrs={'placeholder' : 'URL gambar', 'id' : 'data-img', 'required' : False}), 
            'content' : Textarea(attrs={'placeholder' : 'Konten', 'id' : 'data-content', 'required' : True}),
            'time_posted': DateTimeInput(format='%d/%m/%Y %H:%M', )
        }

