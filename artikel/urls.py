from django.urls import path 
from . import views

app_name = 'artikel'

urlpatterns = [
    path('', views.home, name="home"), 
    path('data/', views.home_data, name="home_data"), 
    path('add/', views.add, name="add"), 
    path('add/data/', views.add_data, name="add_data"), 
    path('details/<int:article_id>/', views.details, name="details"),
    path('details/<int:article_id>/data/', views.details_data, name="details_data"),
    path('delete/<int:article_id>/data/', views.delete_data, name="delete_data"),
    path('edit/<int:article_id>/', views.edit, name="edit"),
    path('edit/<int:article_id>/data/', views.edit_data, name="edit_data"),
]
