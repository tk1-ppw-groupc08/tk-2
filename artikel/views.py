from django.shortcuts import render, redirect 
from .models import Article
from main.models import Profile 
from .forms import ArticleForm
from django.contrib.auth.decorators import login_required
from django.http.response import JsonResponse
import datetime

def home(request):
    return render(request,"artikel/home.html")

def home_data(request):
    article = Article.objects.all()
    article_arr = []
    for a in article : 
        temp = {}
        temp['title'] = a.title
        temp['img'] = a.img
        temp['content'] = a.content
        temp['author'] = a.author.username
        temp['isTenagaMedis'] = a.author.profile.isTenagaMedis
        temp['id'] = a.id
        temp['time_posted'] = a.time_posted
        article_arr.append(temp)
    
    user = {}
    user['username'] = request.user.username

    context = {
        'article_arr' : article_arr,
        'user' : user,  
    }
    return JsonResponse(context)


def details(request, article_id):
    return render(request, 'artikel/details.html')

def details_data(request, article_id):
    obj = Article.objects.get(id=article_id)
    
    context = {}
    context['author'] = obj.author.username
    context['isTenagaMedis'] = obj.author.profile.isTenagaMedis
    context['title'] = obj.title
    context['img'] = obj.img
    context['content'] = obj.content
    context['time_posted'] = obj.time_posted
    
    return JsonResponse(context)

@login_required
def add(request):
    form = ArticleForm(request.POST)
    return render(request, 'artikel/add.html', {'form' : form, } )

def add_data(request): 
    if request.method == 'POST':
        data_title = request.POST.get('title')
        data_img = request.POST.get('img')
        data_content = request.POST.get('content')
        
        context = {}

        post = Article(title=data_title, img=data_img, content=data_content,author=request.user)
        post.save()

        context['title'] = post.title
        context['img'] = post.img 
        context['content'] = post.content
        context['author'] = post.author.username
        context['time'] = post.time_posted
 
        return JsonResponse(context)

def delete_data(request, article_id):
    obj = Article.objects.get(id=article_id)
    obj.delete()
    context = {'status' : 'success'}
    return JsonResponse(context)

@login_required
def edit(request, article_id):
    obj = Article.objects.get(id=article_id)
    form = ArticleForm(instance=obj)
    return render(request, 'artikel/edit.html', {'form': form,})
   
def edit_data(request, article_id):
    obj = Article.objects.get(id=article_id)
    context = {}
    if request.method == 'POST':
        data_title = request.POST.get('title')
        data_img = request.POST.get('img')
        data_content = request.POST.get('content')

        obj.title = data_title
        obj.img = data_img
        obj.content = data_content
        obj.save()
        
        context['title'] = obj.title
        context['img'] = obj.img
        context['content'] = obj.content

        return JsonResponse(context)

